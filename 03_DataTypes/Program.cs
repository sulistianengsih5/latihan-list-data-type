﻿namespace _03_ListDataType
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Implementing Library class that use List
            Book b1 = new Book("Harry Poter");
            Book b2 = new Book("Lord of The Ring");
            Book b3 = new Book("Tru and Nelle");

            Library library = new Library();
            library.addBook(b1);
            library.addBook(b2);
            library.addBook(b3);

            library.remove(0);
            library.remove(b2);

            library.showAll();
        }
    }
}