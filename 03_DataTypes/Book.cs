﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ListDataType
{
    internal class Book
    {
        public string title { get; set; } = default!;

        public Book(string title)
        {
            this.title = title;
        }
    }
}
