﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ListDataType
{
    internal class Library
    {
        public List<Book> bookList;

        public Library()
        {
            this.bookList = new List<Book>();
        }
        
        public void addBook(Book book)
        {
            this.bookList.Add(book);
        }

        public void showAll()
        {
            foreach (Book book in bookList)
            {
                Console.WriteLine(book.title);
            }
        }

        public void remove(int index)
        {
            if (bookList.Count - 1 < index)
            {
                Console.WriteLine("index not found.");
            } else
            {
                this.bookList.RemoveAt(index);
            }  
        }

        public void remove(Book book)
        {
            if (bookList.Contains(book))
            {
                this.bookList.Remove(book);
            } else
            {
                Console.WriteLine("book not found.");
            }
            
        }
    }
}
